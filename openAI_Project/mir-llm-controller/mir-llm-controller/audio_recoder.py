# https://people.csail.mit.edu/hubert/pyaudio/docs/

import pyaudio
import wave
import threading
from pydub import AudioSegment
import sys
import tty


class AudioRecorder:
    def __init__(self, output_filename):
        self.output_filename = output_filename
        self.frames = []
        self.p = pyaudio.PyAudio()
        self.stream = None
        self.recording = False
        self.paused = False
        self.lock = threading.Lock()

    def callback(self, in_data, frame_count, time_info, status):
        if self.recording and not self.paused:
            with self.lock:
                self.frames.append(in_data)
            return in_data, pyaudio.paContinue
        else:
            return b"", pyaudio.paContinue

    def start_recording(self):
        if not self.recording:
            self.frames = []
            self.recording = True
            self.paused = False
            print("Recording...")
            self.stream = self.p.open(
                format=pyaudio.paInt16,
                channels=1,
                rate=44100,
                input=True,
                frames_per_buffer=1024,
                stream_callback=self.callback,
            )
            self.stream.start_stream()

    def pause_recording(self):
        if self.recording and not self.paused:
            self.paused = True
            print("Paused recording.")

    def resume_recording(self):
        if self.recording and self.paused:
            self.paused = False
            print("Resumed recording.")

    def stop_recording(self):
        if self.recording:
            self.recording = False
            if self.stream.is_active():
                self.stream.stop_stream()
                self.stream.close()
                self.p.terminate()
                print("Recording stopped.")
                self.save_audio()

    # def save_audio(self):
    #     if self.frames:
    #         audio_data = b"".join(self.frames)
    #         with wave.open(self.output_filename, "wb") as wf:
    #             wf.setnchannels(1)
    #             wf.setsampwidth(self.p.get_sample_size(pyaudio.paInt16))
    #             wf.setframerate(44100)
    #             wf.writeframes(audio_data)
    #         print(f"Audio saved as {self.output_filename}")

    def save_audio(self):
        if self.frames:
            audio_data = b"".join(self.frames)
            audio_segment = AudioSegment(
                audio_data,
                sample_width=self.p.get_sample_size(pyaudio.paInt16),
                frame_rate=44100,
                channels=1,
            )
            audio_segment.export(self.output_filename, format="mp3")
            print(f"Audio saved as {self.output_filename}")

    def start_stop_redording(self):
        tty.setcbreak(sys.stdin)
        print("\npress any key to start recording.")
        key = ord(sys.stdin.read(1))
        self.start_recording()
        print("press any key to stop recording.")
        key = ord(sys.stdin.read(1))
        self.stop_recording()

    def process_commands_long(self):
        while True:
            command = (
                input("Enter a command (start/pause/resume/stop/quit): ")
                .strip()
                .lower()
            )
            if command == "start":
                self.start_recording()
            elif command == "pause":
                self.pause_recording()
            elif command == "resume":
                self.resume_recording()
            elif command == "stop":
                self.stop_recording()
            elif command == "quit":
                if self.recording:
                    self.stop_recording()
                break
            else:
                print(
                    "Invalid command. Please use 'start', 'pause', 'resume', 'stop', or 'quit'."
                )


if __name__ == "__main__":
    recorder = AudioRecorder("recorded_audio.mp3")
    print("input commands")
    recorder.start_stop_redording()
