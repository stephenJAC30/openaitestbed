import requests, json, uuid
import sys
import re


# Note: the Authorization key is obtained from the link below while inputing the username and password.
# http://192.168.201.196/help/api-documentation

# List of missions
"""
## Retrieve the list of missions that belong to the group with the specified group ID

#  {
#    "url": "/v2.0.0/mission_groups/3c19c64b-9450-11ee-b5aa-000129ae2cd1",
#    "guid": "3c19c64b-9450-11ee-b5aa-000129ae2cd1",
#    "name": "LLM_MiR_API_Control_Map_TUS-ENG-L2-SRI"
#  }

curl -X GET "http://192.168.201.196/api/v2.0.0/mission_groups/3c19c64b-9450-11ee-b5aa-000129ae2cd1/missions" -H "accept: application/json" -H "Authorization: Basic YWRtaW46OGM2OTc2ZTViNTQxMDQxNWJkZTkwOGJkNGRlZTE1ZGZiMTY3YTljODczZmM0YmI4YTgxZjZmMmFiNDQ4YTkxOA==" -H "Accept-Language: en_US"
"""

"""
[
  {
    "url": "/v2.0.0/missions/e03c61c3-9453-11ee-b5aa-000129ae2cd1",
    "guid": "e03c61c3-9453-11ee-b5aa-000129ae2cd1",
    "name": "dock to the charging station"
  },
  {
    "url": "/v2.0.0/missions/29b5c375-9454-11ee-b5aa-000129ae2cd1",
    "guid": "29b5c375-9454-11ee-b5aa-000129ae2cd1",
    "name": "dock to MiR-Roller's home"
  },
  {
    "url": "/v2.0.0/missions/63899c74-9454-11ee-b5aa-000129ae2cd1",
    "guid": "63899c74-9454-11ee-b5aa-000129ae2cd1",
    "name": "go to Roller Pos 2"
  },
  {
    "url": "/v2.0.0/missions/c049c620-9454-11ee-b5aa-000129ae2cd1",
    "guid": "c049c620-9454-11ee-b5aa-000129ae2cd1",
    "name": "go to ARM Pos 2 (Charger)"
  },
  {
    "url": "/v2.0.0/missions/65659640-9455-11ee-b5aa-000129ae2cd1",
    "guid": "65659640-9455-11ee-b5aa-000129ae2cd1",
    "name": "go to UR Home 1"
  },
  {
    "url": "/v2.0.0/missions/8e9725dd-9455-11ee-b5aa-000129ae2cd1",
    "guid": "8e9725dd-9455-11ee-b5aa-000129ae2cd1",
    "name": "go to U203"
  },
  {
    "url": "/v2.0.0/missions/ab3cbd9f-9455-11ee-b5aa-000129ae2cd1",
    "guid": "ab3cbd9f-9455-11ee-b5aa-000129ae2cd1",
    "name": "go to U202"
  },
  {
    "url": "/v2.0.0/missions/c3c9da92-9455-11ee-b5aa-000129ae2cd1",
    "guid": "c3c9da92-9455-11ee-b5aa-000129ae2cd1",
    "name": "go to U204"
  },
  {
    "url": "/v2.0.0/missions/e1e9ec71-9455-11ee-b5aa-000129ae2cd1",
    "guid": "e1e9ec71-9455-11ee-b5aa-000129ae2cd1",
    "name": "go to U206"
  },
  {
    "url": "/v2.0.0/missions/f41daac9-9455-11ee-b5aa-000129ae2cd1",
    "guid": "f41daac9-9455-11ee-b5aa-000129ae2cd1",
    "name": "go to U208"
  },
  {
    "url": "/v2.0.0/missions/0c0df57b-9456-11ee-b5aa-000129ae2cd1",
    "guid": "0c0df57b-9456-11ee-b5aa-000129ae2cd1",
    "name": "go to U209"
  },
  {
    "url": "/v2.0.0/missions/41e7727a-9456-11ee-b5aa-000129ae2cd1",
    "guid": "41e7727a-9456-11ee-b5aa-000129ae2cd1",
    "name": "go to L2 Elevator"
  },
  {
    "url": "/v2.0.0/missions/4492f51d-9457-11ee-b5aa-000129ae2cd1",
    "guid": "4492f51d-9457-11ee-b5aa-000129ae2cd1",
    "name": "play sound Beep"
  },
  {
    "url": "/v2.0.0/missions/948ebc89-9457-11ee-b5aa-000129ae2cd1",
    "guid": "948ebc89-9457-11ee-b5aa-000129ae2cd1",
    "name": "play sound Foghorn"
  },
  {
    "url": "/v2.0.0/missions/cc23b4cf-9457-11ee-b5aa-000129ae2cd1",
    "guid": "cc23b4cf-9457-11ee-b5aa-000129ae2cd1",
    "name": "play sound Horn"
  },
  {
    "url": "/v2.0.0/missions/ea05aa4d-9457-11ee-b5aa-000129ae2cd1",
    "guid": "ea05aa4d-9457-11ee-b5aa-000129ae2cd1",
    "name": "show light Green"
  },
  {
    "url": "/v2.0.0/missions/1ca67e9a-9458-11ee-b5aa-000129ae2cd1",
    "guid": "1ca67e9a-9458-11ee-b5aa-000129ae2cd1",
    "name": "show light Red"
  },
  {
    "url": "/v2.0.0/missions/3b23f49d-9458-11ee-b5aa-000129ae2cd1",
    "guid": "3b23f49d-9458-11ee-b5aa-000129ae2cd1",
    "name": "show light Blue"
  },
  {
    "url": "/v2.0.0/missions/641cdc0c-9823-11ee-bdc9-000129ad3dba",
    "guid": "641cdc0c-9823-11ee-bdc9-000129ad3dba",
    "name": "turn on charging"
  },
  {
    "url": "/v2.0.0/missions/521ec8e4-9a86-11ee-a62c-000129ae2cd1",
    "guid": "521ec8e4-9a86-11ee-a62c-000129ae2cd1",
    "name": "go to U205"
  },
  {
    "url": "/v2.0.0/missions/69d71e7c-9a86-11ee-a62c-000129ae2cd1",
    "guid": "69d71e7c-9a86-11ee-a62c-000129ae2cd1",
    "name": "go to U207"
  },
  {
    "url": "/v2.0.0/missions/233ca0cf-9a95-11ee-b151-000129ad3dba",
    "guid": "233ca0cf-9a95-11ee-b151-000129ad3dba",
    "name": "run pick and place demo"
  }
]
"""


# Get robots info from fleet management
"""
## Retrieve the details about the robot with the specified id
curl -X GET "http://192.168.201.100/api/v2.0.0/robots/1" -H "accept: application/json" -H "Authorization: Basic YWRtaW46OGM2OTc2ZTViNTQxMDQxNWJkZTkwOGJkNGRlZTE1ZGZiMTY3YTljODczZmM0YmI4YTgxZjZmMmFiNDQ4YTkxOA==" -H "Accept-Language: en_US"
"""

"""
{
  "status": {
    "joystick_low_speed_mode_enabled": false,
    "mode_id": 7,
    "moved": 3160.213246696746,
    "mission_queue_id": 0,
    "header": {
      "stamp": {
        "secs": 0,
        "nsecs": 0
      },
      "frame_id": "",
      "seq": 0
    },
    "robot_name": "MiR_201804090_Roller",
    "uptime": 27357,
    "errors": [],
    "unloaded_map_changes": false,
    "distance_to_next_target": 0.21542809903621674,
    "battery_voltage": 0,
    "mode_key_state": "idle",
    "battery_percentage": 91.69999694824219,
    "map_id": "12775490-d91a-11ed-805c-000129ae2cd1",
    "software_version": "2.13.5.4",
    "safety_system_muted": false,
    "mission_text": "Waiting for new missions...",
    "velocity": {
      "linear": 0,
      "angular": 0
    },
    "state_text": "EmergencyStop",
    "position": {
      "y": 14.734330177307129,
      "x": 39.96956253051758,
      "orientation": 176.53506469726562
    },
    "footprint": "[[0.506,-0.32],[0.506,0.32],[-0.454,0.32],[-0.454,-0.32]]",
    "user_prompt": {
      "question": "",
      "has_request": false,
      "timeout": {
        "secs": 0,
        "nsecs": 0
      },
      "user_group": "",
      "guid": "",
      "options": []
    },
    "mode_text": "Mission",
    "hook_status": {
      "trolley": {
        "width": 0,
        "length": 0,
        "offset_locked_wheels": 0,
        "id": 0,
        "height": 0
      },
      "available": false,
      "trolley_attached": false
    },
    "session_id": "ba441516-c97a-11ed-a03a-000129ae2cd1",
    "joystick_web_session_id": "",
    "hook_data": {
      "angle": {
        "timestamp": {
          "secs": 0,
          "nsecs": 0
        },
        "angle": 0
      },
      "height_state": 0,
      "brake_state": 0,
      "height": 0,
      "length": 0,
      "gripper_state": 0
    },
    "battery_time_remaining": 34508,
    "state_id": 10
  },
  "fleet_state_text": "unavailable",
  "allowed_methods": [
    "PUT",
    "GET",
    "DELETE"
  ],
  "description": "",
  "ip": "192.168.201.196",
  "factory_reset_needed": false,
  "created_by_id": "mirconst-guid-0000-0005-users0000000",
  "operation_mode_info": {
    "synchronization_enabled": true,
    "mission_scheduling_enabled": true,
    "resource_management_enabled": true,
    "initial_synchronization_enabled": false,
    "collision_avoidance_enabled": true,
    "mode_name": "ActiveMode",
    "charging_staging_enabled": true,
    "communicator_enabled": true
  },
  "created_by": "/v2.0.0/users/mirconst-guid-0000-0005-users0000000",
  "fleet_state": 1,
  "created_by_name": "Administrator",
  "active": true,
  "serial_number": "201804090",
  "robot_group_id": 4,
  "id": 1,
  "robot_model": "MiR100"
}
"""

# Get maps
"""
curl -X GET "http://192.168.201.100/api/v2.0.0/maps" -H "accept: application/json" -H "Authorization: Basic YWRtaW46OGM2OTc2ZTViNTQxMDQxNWJkZTkwOGJkNGRlZTE1ZGZiMTY3YTljODczZmM0YmI4YTgxZjZmMmFiNDQ4YTkxOA==" -H "Accept-Language: en_US"
"""

"""
[
  {
    "url": "/v2.0.0/maps/mirconst-guid-0000-0001-maps00000000",
    "guid": "mirconst-guid-0000-0001-maps00000000",
    "name": "ConfigurationMap"
  },
  {
    "url": "/v2.0.0/maps/bc55bbcc-c97a-11ed-a03a-000129ae2cd1",
    "guid": "bc55bbcc-c97a-11ed-a03a-000129ae2cd1",
    "name": "Athlone IT"
  },
  {
    "url": "/v2.0.0/maps/12775490-d91a-11ed-805c-000129ae2cd1",
    "guid": "12775490-d91a-11ed-805c-000129ae2cd1",
    "name": "TUS-ENG-L2-SRI"
  },
  {
    "url": "/v2.0.0/maps/3cdd8b9d-d951-11ed-805c-000129ae2cd1",
    "guid": "3cdd8b9d-d951-11ed-805c-000129ae2cd1",
    "name": "TUS-ENG-L1"
  }
]
"""

# Retrieve the list of positions that belong to the map with the specified map ID
"""
curl -X GET "http://192.168.201.100/api/v2.0.0/maps/12775490-d91a-11ed-805c-000129ae2cd1/positions" -H "accept: application/json" -H "Authorization: Basic YWRtaW46OGM2OTc2ZTViNTQxMDQxNWJkZTkwOGJkNGRlZTE1ZGZiMTY3YTljODczZmM0YmI4YTgxZjZmMmFiNDQ4YTkxOA==" -H "Accept-Language: en_US"
"""

"""
[
  {
    "url": "/v2.0.0/positions/b3a406f5-d932-11ed-805c-000129ae2cd1",
    "map": "/v2.0.0/maps/12775490-d91a-11ed-805c-000129ae2cd1",
    "guid": "b3a406f5-d932-11ed-805c-000129ae2cd1",
    "name": "U209",
    "type_id": 0
  },
  {
    "url": "/v2.0.0/positions/c5bf1351-d932-11ed-805c-000129ae2cd1",
    "map": "/v2.0.0/maps/12775490-d91a-11ed-805c-000129ae2cd1",
    "guid": "c5bf1351-d932-11ed-805c-000129ae2cd1",
    "name": "U206",
    "type_id": 0
  },
  {
    "url": "/v2.0.0/positions/e4027916-d932-11ed-805c-000129ae2cd1",
    "map": "/v2.0.0/maps/12775490-d91a-11ed-805c-000129ae2cd1",
    "guid": "e4027916-d932-11ed-805c-000129ae2cd1",
    "name": "U203",
    "type_id": 0
  },
  {
    "url": "/v2.0.0/positions/3c08776a-da0a-11ed-bb45-000129ae2cd1",
    "map": "/v2.0.0/maps/12775490-d91a-11ed-805c-000129ae2cd1",
    "guid": "3c08776a-da0a-11ed-bb45-000129ae2cd1",
    "name": "VL Conveyor 1",
    "type_id": 11
  },
  {
    "url": "/v2.0.0/positions/d550a47a-340a-4e62-9fd6-19c81b5b4354",
    "map": "/v2.0.0/maps/12775490-d91a-11ed-805c-000129ae2cd1",
    "guid": "d550a47a-340a-4e62-9fd6-19c81b5b4354",
    "name": "VL Conveyor 1",
    "type_id": 12
  },
  {
    "url": "/v2.0.0/positions/ebd44eb4-da11-11ed-bb45-000129ae2cd1",
    "map": "/v2.0.0/maps/12775490-d91a-11ed-805c-000129ae2cd1",
    "guid": "ebd44eb4-da11-11ed-bb45-000129ae2cd1",
    "name": "Charger 24V 1",
    "type_id": 7
  },
  {
    "url": "/v2.0.0/positions/c8349981-f235-474d-b90d-e0afef23d325",
    "map": "/v2.0.0/maps/12775490-d91a-11ed-805c-000129ae2cd1",
    "guid": "c8349981-f235-474d-b90d-e0afef23d325",
    "name": "Charger 24V 1",
    "type_id": 8
  },
  {
    "url": "/v2.0.0/positions/1934cf89-da12-11ed-bb45-000129ae2cd1",
    "map": "/v2.0.0/maps/12775490-d91a-11ed-805c-000129ae2cd1",
    "guid": "1934cf89-da12-11ed-bb45-000129ae2cd1",
    "name": "U204",
    "type_id": 0
  },
  {
    "url": "/v2.0.0/positions/8381ebf9-da12-11ed-bb45-000129ae2cd1",
    "map": "/v2.0.0/maps/12775490-d91a-11ed-805c-000129ae2cd1",
    "guid": "8381ebf9-da12-11ed-bb45-000129ae2cd1",
    "name": "U202",
    "type_id": 0
  },
  {
    "url": "/v2.0.0/positions/7f0e72c0-da18-11ed-bb45-000129ae2cd1",
    "map": "/v2.0.0/maps/12775490-d91a-11ed-805c-000129ae2cd1",
    "guid": "7f0e72c0-da18-11ed-bb45-000129ae2cd1",
    "name": "L2 Elevator",
    "type_id": 0
  },
  {
    "url": "/v2.0.0/positions/c0146917-da23-11ed-bf64-000129ad3dba",
    "map": "/v2.0.0/maps/12775490-d91a-11ed-805c-000129ae2cd1",
    "guid": "c0146917-da23-11ed-bf64-000129ad3dba",
    "name": "ARM Pos 2 (Charger)",
    "type_id": 0
  },
  {
    "url": "/v2.0.0/positions/b2e7feda-e5c9-11ed-925f-000129ad3dba",
    "map": "/v2.0.0/maps/12775490-d91a-11ed-805c-000129ae2cd1",
    "guid": "b2e7feda-e5c9-11ed-925f-000129ad3dba",
    "name": "UR Home 1",
    "type_id": 0
  },
  {
    "url": "/v2.0.0/positions/30372c44-e611-11ed-8a7d-a4ae111c19c7",
    "map": "/v2.0.0/maps/12775490-d91a-11ed-805c-000129ae2cd1",
    "guid": "30372c44-e611-11ed-8a7d-a4ae111c19c7",
    "name": "Roller Pos 2",
    "type_id": 0
  },
  {
    "url": "/v2.0.0/positions/e91bc732-719c-11ee-bb2b-000129ae2cd1",
    "map": "/v2.0.0/maps/12775490-d91a-11ed-805c-000129ae2cd1",
    "guid": "e91bc732-719c-11ee-bb2b-000129ae2cd1",
    "name": "U208",
    "type_id": 0
  }
]
"""

mir_top_roller_url = "http://192.168.201.196/api/v2.0.0/"
mir_top_ur5_url = "http://192.168.201.189/api/v2.0.0/"
mir_fleet_management_url = "http://192.168.201.100/api/v2.0.0/"
mir_llm_mission_group_name = "LLM_MiR_API_Control_Map_TUS-ENG-L2-SRI"
mir_rest_headers = {
    "Content-Type": "application/json",
    "Accept-Language": "en_US",
    "Authorization": "Basic YWRtaW46OGM2OTc2ZTViNTQxMDQxNWJkZTkwOGJkNGRlZTE1ZGZiMTY3YTljODczZmM0YmI4YTgxZjZmMmFiNDQ4YTkxOA==",
}
mir_missions = {
    "go to": [
        "Roller Pos 2",
        "ARM Pos 2 (Charger)",
        "UR Home 1",
        "U202",
        "U203",
        "U204",
        "U205",
        "U206",
        "U207",
        "U208",
        "U209",
        "L2 Elevator",
    ],
    "dock to": ["MiR-Roller's home", "the charging station"],
    "play sound": ["Beep", "Horn", "Foghorn"],
    "show light": ["Red", "Green", "Blue"],
    "run": ["pick and place demo"],
}

restricted_missions = {
    "MiR Roller": ["run pick and place demo"],
    "MiR UR5": ["dock to MiR-Roller's home"]
}

mir_names = {
    # name in MiRFleet: name in the programme
    "MiR_201804090_Roller": "MiR Roller",
    "MiR_201804085_UR5": "MiR UR5",
    "MiRFleet": "MiR Fleet Manager",
}


class MiR_REST_API_Common:
    """
    Initialise the REST API with a specific mission group only.
    """

    def __init__(
        self,
        mir_rest_url=mir_fleet_management_url,
        mission_group_name=mir_llm_mission_group_name,
    ):
        self.mir_rest_url = mir_rest_url
        self.mir_rest_headers = mir_rest_headers
        self.rest_api_timeout = 1
        self.mission_groups, self.missions, self.maps, self.positions = {}, {}, {}, {}
        self.active_map_name = "TUS-ENG-L2-SRI"
        self.get_all_mission_groups()
        self.get_all_missions_from_mission_group(mission_group_name)
        self.get_all_maps()
        self.get_positions_on_map(self.active_map_name)

    def rest_api_get(self, request_url, headers):
        try:
            response = requests.get(
                request_url, headers=headers, timeout=self.rest_api_timeout
            )
            return response
        except requests.exceptions.Timeout:
            print("The GET request timed out. The web service might be offline.")
        except requests.exceptions.RequestException as e:
            print(f"An error occurred: {e}")

    def rest_api_post(self, post_url, json, headers):
        try:
            response = requests.post(
                post_url, json=json, headers=headers, timeout=self.rest_api_timeout
            )
            return response
        except requests.exceptions.Timeout:
            print("The POST request timed out. The web service might be offline.")
        except requests.exceptions.RequestException as e:
            print(f"An error occurred: {e}")

    def get_mir_response(self, request_url):
        response = self.rest_api_get(request_url, headers=self.mir_rest_headers)
        if response:
            return {item["name"]: item["guid"] for item in response.json()}

    def get_all_mission_groups(self):
        # curl -X GET "http://192.168.201.196/api/v2.0.0/mission_groups" -H "accept: application/json" -H "Authorization: Basic YWRtaW46OGM2OTc2ZTViNTQxMDQxNWJkZTkwOGJkNGRlZTE1ZGZiMTY3YTljODczZmM0YmI4YTgxZjZmMmFiNDQ4YTkxOA==" -H "Accept-Language: en_US"
        self.mission_groups = self.get_mir_response(
            self.mir_rest_url + "mission_groups"
        )

    def get_mission_group_guid_by_name(self, mission_group_name):
        if self.mission_groups:
            return self.mission_groups[mission_group_name]

    def get_all_missions_from_mission_group(self, mission_group_name):
        mission_group_guid = self.get_mission_group_guid_by_name(mission_group_name)
        # curl -X GET "http://192.168.201.196/api/v2.0.0/mission_groups/3c19c64b-9450-11ee-b5aa-000129ae2cd1/missions" -H "accept: application/json" -H "Authorization: Basic YWRtaW46OGM2OTc2ZTViNTQxMDQxNWJkZTkwOGJkNGRlZTE1ZGZiMTY3YTljODczZmM0YmI4YTgxZjZmMmFiNDQ4YTkxOA==" -H "Accept-Language: en_US"
        if mission_group_guid:
            self.missions = self.get_mir_response(
                self.mir_rest_url + "mission_groups/" + mission_group_guid + "/missions"
            )

    def get_mission_guid_by_name(self, mission_name):
        if self.missions:
            return self.missions[mission_name]

    def get_all_maps(self):
        # curl -X GET "http://192.168.201.100/api/v2.0.0/maps" -H "accept: application/json" -H "Authorization: Basic YWRtaW46OGM2OTc2ZTViNTQxMDQxNWJkZTkwOGJkNGRlZTE1ZGZiMTY3YTljODczZmM0YmI4YTgxZjZmMmFiNDQ4YTkxOA==" -H "Accept-Language: en_US"
        self.maps = self.get_mir_response(self.mir_rest_url + "maps")

    def get_map_guid_by_name(self, map_name):
        if self.maps:
            return self.maps[map_name]

    def get_positions_on_map(self, map_name):
        # curl -X GET "http://192.168.201.100/api/v2.0.0/maps/12775490-d91a-11ed-805c-000129ae2cd1/positions" -H "accept: application/json" -H "Authorization: Basic YWRtaW46OGM2OTc2ZTViNTQxMDQxNWJkZTkwOGJkNGRlZTE1ZGZiMTY3YTljODczZmM0YmI4YTgxZjZmMmFiNDQ4YTkxOA==" -H "Accept-Language: en_US"
        map_guid = self.get_map_guid_by_name(map_name)
        if map_guid:
            self.positions = self.get_mir_response(
                self.mir_rest_url + "maps/" + map_guid + "/positions"
            )


class MiR_Robot_Controller(MiR_REST_API_Common):
    def __init__(
        self,
        mir_rest_url=mir_top_roller_url,
        mission_group_name=mir_llm_mission_group_name,
    ):
        super().__init__(mir_rest_url, mission_group_name)
        self.is_robot = True

    def post_to_mir(self, post_url, payload):
        return self.rest_api_post(post_url, json=payload, headers=self.mir_rest_headers)

    def post_to_mission_queue(self, mission_guid):
        payload = {"mission_id": mission_guid}
        post_url = self.mir_rest_url + "mission_queue"
        return self.post_to_mir(post_url, payload)

    def post_to_mission_queue_by_name(self, mission_name):
        mission_guid = self.get_mission_guid_by_name(mission_name)
        self.post_to_mission_queue(mission_guid)


class MiR_Fleet_Manager(MiR_REST_API_Common):
    def __init__(
        self,
        mir_rest_url=mir_fleet_management_url,
        mission_group_name=mir_llm_mission_group_name,
    ):
        super().__init__(mir_rest_url, mission_group_name)
        self.robots = {}
        self.get_all_robots_info()
        self.is_robot = False

    def get_all_robots_info(self):
        # curl -X GET "http://192.168.201.100/api/v2.0.0/robots" -H "accept: application/json" -H "Authorization: Basic YWRtaW46OGM2OTc2ZTViNTQxMDQxNWJkZTkwOGJkNGRlZTE1ZGZiMTY3YTljODczZmM0YmI4YTgxZjZmMmFiNDQ4YTkxOA==" -H "Accept-Language: en_US"
        request_url = self.mir_rest_url + "robots"
        # response = requests.get(request_url, headers=self.mir_rest_headers)
        response = self.rest_api_get(request_url, headers=self.mir_rest_headers)
        if not response:
            return
        robot_ids = {item["id"] for item in response.json()}
        # curl -X GET "http://192.168.201.100/api/v2.0.0/robots/1" -H "accept: application/json" -H "Authorization: Basic YWRtaW46OGM2OTc2ZTViNTQxMDQxNWJkZTkwOGJkNGRlZTE1ZGZiMTY3YTljODczZmM0YmI4YTgxZjZmMmFiNDQ4YTkxOA==" -H "Accept-Language: en_US"
        self.robots = {}
        for id in robot_ids:
            request_url = self.mir_rest_url + "robots/" + str(id)
            response = requests.get(request_url, headers=self.mir_rest_headers)
            response_json = response.json()
            self.robots[
                mir_names[response_json["status"]["robot_name"]]
            ] = response_json

    def is_robot_available(self, robot_name):
        self.get_all_robots_info()
        if self.robots:
            status = self.robots[robot_name]["fleet_state_text"]
            return status != "unavailable"
        else:
            return False
