import requests, json, uuid
import sys
import re

from mir_rest_api_controller import *

# Send Restful Commands to MiR
# LLM_ONLY = False 

# Do not send actual commands to MiR. Turn the option on to debug the LLM. 
LLM_ONLY = True 


class MiR_LMP_Interface:
    def __init__(self):
        if LLM_ONLY:
            return
        self.mir_ur5_controller = MiR_Robot_Controller(mir_top_ur5_url)
        self.mir_roller_controller = MiR_Robot_Controller(mir_top_roller_url)
        self.mir_fleet_manager = MiR_Fleet_Manager(mir_fleet_management_url)

        self.mir_controllers = {
            "MiR UR5": self.mir_ur5_controller,
            "MiR Roller": self.mir_roller_controller,
            "MiR Fleet Manager": self.mir_fleet_manager,
        }

    def is_robot_available(self, robot_name):
        if LLM_ONLY:
            return False
        robot_controller = self.mir_controllers[robot_name]
        if not robot_controller.is_robot:
            print("I am not a robot.")
            return False
        if not self.mir_fleet_manager.is_robot_available(robot_name):
            print("The robot is not available.")
            return False
        return True
    
    def is_mission_restricted(self, robot_name, mission_name):
        if mission_name in restricted_missions[robot_name]:
            print(f"Robot {robot_name} is not allowed to perform the mission '{mission_name}'")
            return True
        return False

    def go_to_position(self, robot_name, position_name):
        """
        Args:
          robot_name: is defined in mir_controllers.
          position_name: is defined in mir_missions.
        """
        if LLM_ONLY:
            print(f"MiR_LMP_Interface: go_to_position({robot_name}, {position_name})")
            return False
        if not self.is_robot_available(robot_name):
            return
        robot_controller = self.mir_controllers[robot_name]
        if position_name in mir_missions["go to"]:
            mission_name = "go to " + position_name
        elif position_name in self.mir_missions["dock to"]:
            mission_name = "dock to " + position_name
        else:
            print("The position is not defined.")
            return
        if self.is_mission_restricted(robot_name, mission_name):
            return
        print(mission_name)
        robot_controller.post_to_mission_queue_by_name(mission_name)

    def dock_to_position(self, robot_name, position_name):
        """
        Args:
          robot_name: is defined in mir_controllers.
          position_name: is defined in mir_missions.
        """
        if LLM_ONLY:
            print(f"MiR_LMP_Interface: dock_to_position({robot_name}, {position_name})")
            return False
        if not self.is_robot_available(robot_name):
            return False
        robot_controller = self.mir_controllers[robot_name]
        mission_name = "dock to " + position_name
        if self.is_mission_restricted(robot_name, mission_name):
            return
        print(mission_name)
        robot_controller.post_to_mission_queue_by_name(mission_name)

    def play_sound(self, robot_name, sound_name):
        """
        Args:
          robot_name: is defined in mir_controllers.
          sound_name: is defined in mir_missions.
        """
        if LLM_ONLY:
            print(f"MiR_LMP_Interface: play_sound({robot_name}, {sound_name})")
            return False
        if not self.is_robot_available(robot_name):
            return False
        robot_controller = self.mir_controllers[robot_name]
        if sound_name not in mir_missions["play sound"]:
            print("The sound is not defined.")
            return
        mission_name = "play sound " + sound_name
        print(mission_name)
        robot_controller.post_to_mission_queue_by_name(mission_name)

    def show_light(self, robot_name, light_name):
        """
        Args:
          robot_name: is defined in mir_controllers.
          light_name: is defined in mir_missions.
        """
        if LLM_ONLY:
            print(f"MiR_LMP_Interface: show_light({robot_name}, {light_name})")
            return False
        if not self.is_robot_available(robot_name):
            return False
        robot_controller = self.mir_controllers[robot_name]
        if light_name not in mir_missions["show light"]:
            print("The sound is not defined.")
            return
        mission_name = "show light " + light_name
        print(mission_name)
        robot_controller.post_to_mission_queue_by_name(mission_name)

    def run_program(self, robot_name, program_name):
        """
        Args:
          robot_name: is defined in mir_controllers.
          program_name: Programme to run.
        """
        if LLM_ONLY:
            print(f"MiR_LMP_Interface: run_program({robot_name}, {program_name})")
            return False
        if program_name not in mir_missions["run"]:
            print("The programe is not defined.")
            return
        robot_controller = self.mir_controllers[robot_name]
        mission_name = "run " + program_name
        if self.is_mission_restricted(robot_name, mission_name):
            return
        print(mission_name)
        robot_controller.post_to_mission_queue_by_name(mission_name)

    def get_position_names(self):
        if LLM_ONLY:
            print(f"MiR_LMP_Interface: get_position_names()")
            return []
        return sorted(self.mir_fleet_manager.positions.keys())

    def get_room_names(self):
        if LLM_ONLY:
            print(f"MiR_LMP_Interface: get_room_names()")
            return ["U202", "U203", "U204", "U205", "U206", "U207", "U208", "U209"]
        rooms = [
            key
            for key in self.mir_fleet_manager.positions.keys()
            if key.startswith("U2")
        ]
        return sorted(rooms)


if __name__ == "__main__":
    mir_lmp_interface = MiR_LMP_Interface()

    # Robot "MiR UR5" go to "U203"
    # mir_lmp_interface.go_to_position("MiR UR5", "UR Home 1")
    # mir_lmp_interface.go_to_position("MiR Roller", "U203")
    # mir_lmp_interface.go_to_position("MiR Fleet Manager", "U203")

    # mir_lmp_interface.dock_to_position("MiR UR5", "the charging station")
    # mir_lmp_interface.dock_to_position("MiR Roller", "the charging station")
    # mir_llm_wrapper.dock_to_position("MiR Fleet Manager", "the charging station")

    # mir_lmp_interface.play_sound("MiR UR5", "Beep")
    # mir_lmp_interface.play_sound("MiR Roller", "Beep")
    # mir_lmp_interface.play_sound("MiR Fleet Manager", "Beep")

    # mir_lmp_interface.show_light("MiR UR5", "Red")
    # mir_lmp_interface.show_light("MiR Roller", "Red")
    # mir_lmp_interface.show_light("MiR Fleet Manager", "Red")

    # ['ARM Pos 2 (Charger)', 'Charger 24V 1', 'L2 Elevator', 'Roller Pos 2', 'U202', 'U203', 'U204', 'U205', 'U206', 'U207', 'U208', 'U209', 'UR Home 1', 'VL Conveyor 1']
    # print(mir_lmp_interface.get_position_names())
    # ['U202', 'U203', 'U204', 'U205', 'U206', 'U207', 'U208', 'U209']
    # print(mir_lmp_interface.get_room_names())
