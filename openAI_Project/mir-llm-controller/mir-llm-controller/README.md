MiR Controller using LLM
------------------------

This folder contains the implementation for controlling MiR robots using LLM based on Google Code As Policy.  

## Usage

Tested with python-3.9.  

- Run the main programme  
```bash
python mir_lmp_controller_ui.py text 
python mir_lmp_controller_ui.py audio
```

- Note: use the option inside mir_lmp_interface.py to control whether send the Restful commands to MiR or not. 

```Python
# Send Restful Commands to MiR
# LLM_ONLY = False 

# Do not send actual commands to MiR. Turn the option on to debug the LLM. 
LLM_ONLY = True 
```

- Test the LLM / MiR interface  
```bash
python mir_lmp_interface.py
```

## Code structure

- mir_lmp_controller_ui.py: The main entrance.  
- mir_lmp_interface.py: The bridge between the LMP and the MiR Restful APIs.  
- mir_rest_api_controller.py: The Restful APIs to call MiR robot functions.  
- audio_recoder.py: Audio recoder.  
- lmp.py: Implementation of Language Model Programs (LMPs) based on Google Code as Policy. 
