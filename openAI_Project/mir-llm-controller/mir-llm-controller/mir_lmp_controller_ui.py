import os
import sys
import copy
import numpy as np
import pygame

from openai import OpenAI

openai_client = OpenAI()
openai_api_key = os.environ.get("OPENAI_API_KEY")
# model_name = 'code-davinci-002' # 'text-davinci-002'
model_name = "gpt-3.5-turbo"

from lmp import *
from mir_lmp_interface import *

from audio_recoder import *

## LMP Prompts

prompt_mir_lmp_controller_ui = """
# Python MiR Autonomous Mobile Robot control script
import numpy as np
from env_utils import go_to_position dock_to_position play_sound show_light run_program
from plan_utils import parse_position

# Let robot MiR UR5 go to position UR Home 1.
say('OK - Requesting MiR UR5 to move to UR Home 1.')
go_to_position('MiR UR5, 'UR Home 1')

# UR5, go home
say('OK - Requesting MiR UR5 to move to its home position.')
go_to_position('MiR UR5, 'UR Home 1')

# Roller, go to room U203.
say('OK - Requesting MiR Roller to move to U203.')
go_to_position('MiR Roller', 'U203')

# MiR Fleet Manager go to room U202.
say('No - MiR Fleet Manager is not a robot. It cannot move!')

# ur5 dockes to the charging station
say('OK - Requesting MiR UR5 to dock to the charging station.')
dock_to_position('MiR UR5', 'the charging station')

# roller. dock to your home.
say('OK - Requesting MiR Roller to dock to its home.')
dock_to_position('MiR Roller', 'MiR-Roller's home')

# Roller, go home.
say('OK - Requesting MiR Roller to dock to its home.')
dock_to_position('MiR Roller', 'MiR-Roller's home')

# Let MiR Fleet Manager dock to the charging station.
say('No - MiR Fleet Manager is not a robot. It cannot move!')

# MiR UR5 plays the Beep sound
say('OK - Requesting MiR UR5 to play the Beep sound.')
play_sound('MiR UR5', 'Beep')

# Roller. horn.
say('OK - Requesting MiR Roller to play the Horn sound.')
play_sound('MiR Roller', 'Horn')

# MiR UR5, play the Foghorn sound. 
say('OK - Requesting MiR UR5 to play the Foghorn sound.')
play_sound('MiR UR5', 'Foghorn')

# ur5, play the Foghorn sound 4 times.
say('OK - Requesting MiR UR5 to play the Foghorn sound 4 times.')
for _ in range(4):
    play_sound('MiR UR5', 'Foghorn')

# MiR Fleet Manager plays the Foghorn sound.
say('No - MiR Fleet Manager is not a robot. It cannot play sound!')

# Let robot 'MiR UR5' show red light.
say('OK - Requesting MiR UR5 to show the Red light.')
show_light('MiR UR5', 'Red')

# Roller show green light. 
say('OK - Requesting MiR Roller to show the Green light.')
show_light('MiR Roller', 'Green')

# Let 'MiR Fleet Manager' show blue light. 
say('No - MiR Fleet Manager is not a robot. It cannot show light!')

# UR5 go to room U206 and then show red light.
say('OK - Requesting MiR UR5 to go to room U206 and then show the Red light.')
go_to_position('MiR UR5', 'U206')
show_light('MiR UR5', Red')

# roller, show red light 10 times
say('OK - Requesting MiR Roller to show Red light 10 times.')
for _ in range(10):
    show_light('MiR Roller', 'Red')

# UR5. flash red light 10 times.
say('OK - Requesting MiR UR5 to flash Red light 10 times.')
for _ in range(10):
    show_light('MiR UR5', 'Red')
    
# UR5, run the pick and place demo.
say('OK - Requesting MiR UR5 to run the pick and place demo.')
run_program('MiR UR5', 'pick and place demo')

# MiR UR5 shows red light and MiR Roller goes to room U209.
say('OK - Requesting MiR UR5 to show Red light and MiR Roller to go to room U209.')
show_light('MiR UR5', 'Red')
go_to_position('MiR Roller', 'U209')

# UR5, go to the room between U202 and U204.
target_positions = parse_position('the room between U202 and U204')
if not target_positions:
    say('No - It is not possible to locate the room')
else:
    for pos in target_positions:
        say(f'OK - Requesting MiR UR5 to move to {pos}')
        go_to_position('MiR UR5', pos)

# roller, go to the room between U202 and U204.
target_positions = parse_position('the room between U202 and U204')
if not target_positions:
    say('No - It is not possible to locate the room')
else:
    for pos in target_positions:
        say(f'OK - Requesting MiR Roller to move to {pos}')
        go_to_position('MiR Roller', pos)

# roller, go to a single room between U202 and U206.
target_positions = parse_position('a single room between U202 and U206')
if not target_positions:
    say('No - It is not possible to locate the room')
else:
    for pos in target_positions:
        say(f'OK - Requesting MiR Roller to move to {pos}')
        go_to_position('MiR Roller', pos)
        
# roller, go to any random number of rooms between U202 and U209.
target_positions = parse_position('a random number of rooms between U202 and U209')
if not target_positions:
    say('No - It is not possible to locate the room')
else:
    for pos in target_positions:
        say(f'OK - Requesting MiR Roller to move to {pos}')
        go_to_position('MiR Roller', pos)

# Roller, go to the room with maximum room number.
target_positions = parse_position('the room with maximum room number')
if not target_positions:
    say('No - It is not possible to locate the room')
else:
    for pos in target_positions:
        say(f'OK - Requesting MiR Roller to move to {pos}')
        go_to_position('MiR Roller', pos)
""".strip()

# ['U202', 'U203', 'U204', 'U206', 'U208', 'U209']
# ['ARM Pos 2 (Charger)', 'Charger 24V 1', 'L2 Elevator', 'Roller Pos 2', 'U202', 'U203', 'U204', 'U206', 'U208', 'U209', 'UR Home 1', 'VL Conveyor 1']
prompt_parse_position = """
# This script needs to return an array of positions.

import numpy as np
from env_utils import get_room_names get_position_names

# All rooms
ret_val = get_room_names()

# The room between U203 and U206.
ret_val = [room for room in get_room_names() if "U203" < room < "U206"]

# The rooms between U206 and U209
ret_val = [room for room in get_room_names() if "U206" < room < "U209"]

# a single random room between U202 and U209
# import np.random as random
ret_val = [np.random.choice([for room in get_room_names() if "U202" < room < "U209"])]

# A random number of rooms between U202 and U209.
# import np.random as random
ret_val = [np.random.choice([for room in get_room_names() if "U202" < room < "U209"], size=2, replace=False)]

# The room with maximum room number
max_room = max(get_room_names(), key=lambda room: int(room[1:]))
ret_val = [max_room]
""".strip()

prompt_fgen = """
import numpy as np
from shapely.geometry import *
from shapely.affinity import *

from env_utils import get_obj_pos, get_obj_names
from ctrl_utils import put_first_on_second

# define function: total = get_total(xs=numbers).
def get_total(xs):
    return np.sum(xs)

# define function: y = eval_line(x, slope, y_intercept=0).
def eval_line(x, slope, y_intercept):
    return x * slope + y_intercept

# define function: pt = get_pt_to_the_left(pt, dist).
def get_pt_to_the_left(pt, dist):
    return pt + [-dist, 0]

# define function: pt = get_pt_to_the_top(pt, dist).
def get_pt_to_the_top(pt, dist):
    return pt + [0, dist]

# define function line = make_line_by_length(length=x).
def make_line_by_length(length):
  line = LineString([[0, 0], [length, 0]])
  return line

# define function: line = make_vertical_line_by_length(length=x).
def make_vertical_line_by_length(length):
  line = make_line_by_length(length)
  vertical_line = rotate(line, 90)
  return vertical_line

# define function: pt = interpolate_line(line, t=0.5).
def interpolate_line(line, t):
  pt = line.interpolate(t, normalized=True)
  return np.array(pt.coords[0])

# example: scale a line by 2.
line = make_line_by_length(1)
new_shape = scale(line, xfact=2, yfact=2)

# example: put object1 on top of object0.
put_first_on_second('object1', 'object0')

# example: get the position of the first object.
obj_names = get_obj_names()
pos_2d = get_obj_pos(obj_names[0])
""".strip()


## LMP Config

cfg_mir_lmp = {
    "lmps": {
        "mir_lmp_controller_ui": {
            "prompt_text": prompt_mir_lmp_controller_ui,
            "engine": model_name,
            "max_tokens": 512,
            "temperature": 0,
            "query_prefix": "# ",
            "query_suffix": ".",
            "stop": ["#", "objects = ["],
            "maintain_session": True,
            "debug_mode": False,
            "include_context": True,
            "has_return": False,
            "return_val_name": "ret_val",
        },
        "parse_position": {
            "prompt_text": prompt_parse_position,
            "engine": model_name,
            "max_tokens": 512,
            "temperature": 0,
            "query_prefix": "# ",
            "query_suffix": ".",
            "stop": ["#"],
            "maintain_session": False,
            "debug_mode": False,
            "include_context": True,
            "has_return": True,
            "return_val_name": "ret_val",
        },
        "fgen": {
            "prompt_text": prompt_fgen,
            "engine": model_name,
            "max_tokens": 512,
            "temperature": 0,
            "query_prefix": "# define function: ",
            "query_suffix": ".",
            "stop": ["# define", "# example"],
            "maintain_session": False,
            "debug_mode": False,
            "include_context": True,
        },
    }
}

## LMP Utils

# Interactive Tabletop Manipulation


def play_mp3(mp3_file):
    pygame.mixer.init()
    pygame.mixer.music.load(mp3_file)
    pygame.mixer.music.play()
    # pygame.time.wait(3000)  # Adjust the time (in milliseconds) as needed
    while pygame.mixer.music.get_busy():
        pygame.time.Clock().tick(10)


def speak(msg):
    speech_file_path = "robot_speech.mp3"
    response = openai_client.audio.speech.create(
        model="tts-1", voice="alloy", input=msg
    )
    response.stream_to_file(speech_file_path)
    play_mp3(speech_file_path)


def say(msg):
    print(f"robot says: {msg}")
    if input_type == "audio":
        speak(msg)


def setup_LMP(cfg_mir_lmp):
    # LMP env wrapper
    cfg_mir_lmp = copy.deepcopy(cfg_mir_lmp)

    # qys. Setup the env variables that will be used by LMP.
    # qys. LMP <==> LMP_wrapper <==> env
    mir_lmp_interface = MiR_LMP_Interface()

    # creating APIs that the LMPs can interact with
    fixed_vars = {"np": np}

    # qys. Mapping from a function name to the function object defined in LMP_wrapper.
    variable_vars = {
        k: getattr(mir_lmp_interface, k)
        for k in dir(mir_lmp_interface) if callable(getattr(mir_lmp_interface, k)) and not k.startswith("_")
    }  # our custom APIs exposed to LMPs

    # variable_vars = {
    #     k: getattr(mir_lmp_interface, k)
    #     for k in [
    #         "go_to_position",
    #         "dock_to_position",
    #         "play_sound",
    #         "show_light",
    #         "run_program",
    #         "get_position_names",
    #         "get_room_names",
    #     ]
    # }
    
    # variable_vars["say"] = lambda msg: print(f"robot says: {msg}")
    variable_vars["say"] = say

    # creating the function-generating LMP
    # qys. LMPFGen: generate functions from signatures or from code
    lmp_fgen = LMPFGen(
        cfg_mir_lmp["lmps"]["fgen"], fixed_vars, variable_vars, openai_client
    )

    # creating other low-level LMPs
    # qys. LMP: generate code from user prompts, and execute the code.
    variable_vars.update(
        {
            k: LMP(
                k,
                cfg_mir_lmp["lmps"][k],
                lmp_fgen,
                fixed_vars,
                variable_vars,
                openai_client,
            )
            for k in [
                "parse_position",
            ]
        }
    )

    # creating the LMP that deals w/ high-level language commands
    mir_lmp = LMP(
        "mir_lmp_controller_ui",
        cfg_mir_lmp["lmps"]["mir_lmp_controller_ui"],
        lmp_fgen,
        fixed_vars,
        variable_vars,
        openai_client,
    )

    return mir_lmp


def get_user_input_by_text():
    return input(
        "\nRooms: ['U202', 'U203', 'U204', 'U205', 'U206', 'U207', 'U208', 'U209']\nEnter a command to control the robots - 'MiR UR5' and 'MiR Roller'.\nExpmple: UR5, show red light.\nInput: "
    )


def get_user_input_by_audio():
    speech_file_path = "user_speech.mp3"

    print(
        "\nRooms: ['U202', 'U203', 'U204', 'U205', 'U206', 'U207', 'U208', 'U209']\nSpeek a command to control the robots - 'MiR UR5' and 'MiR Roller'.\nExpmple: UR5, show red light."
    )
    recorder = AudioRecorder(speech_file_path)
    recorder.start_stop_redording()

    audio_file = open(speech_file_path, "rb")
    transcript = openai_client.audio.transcriptions.create(
        model="whisper-1", file=audio_file, response_format="text"
    )
    return transcript


def usage():
    print(f"Usage: {sys.argv[0]} <input_type>")
    print("Input types: 'text' or 'audio'")
    sys.exit(1)


if __name__ == "__main__":
    if len(sys.argv) != 2:
        usage()
    input_type = sys.argv[1]

    mir_lmp_ui = setup_LMP(cfg_mir_lmp)

    while True:
        # print("Use CTRL-C to quit the loop.")
        if input_type == "text":
            user_input = get_user_input_by_text()
        elif input_type == "audio":
            user_input = get_user_input_by_audio()
        else:
            usage()
        print(f"\nuser_input: '{user_input}'")

        mir_lmp_ui(user_input.strip())
